// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

/**
 * @title MoonCatResolver
 * @notice ENS Resolver for MoonCat subdomains
 * @dev Auto-updates to point to the owner of that specific MoonCat
 */
interface IMoonCatNameService {

    //address payable public owner;
    function owner() external view returns (address);

    //bytes32 public immutable rootHash;
    function rootHash() external view returns (bytes32);

    //string public ENSDomain;
    /**
     * @dev Reference for the ENS domain this contract resolves
     */
    function ENSDomain() external view returns (string memory);

    //string public avatarBaseURI;
    function avatarBaseURI() external view returns (string memory);

    //uint64 public defaultTTL;
    function defaultTTL() external view returns (uint64);

    /* Events */
    event AddrChanged(bytes32 indexed node, address a);
    event AddressChanged(bytes32 indexed node, uint coinType, bytes newAddress);
    event TextChanged(bytes32 indexed node, string indexedKey, string key);
    event ContenthashChanged(bytes32 indexed node, bytes hash);

    /**
     * @dev Allow current `owner` to transfer ownership to another address
     */
    function transferOwnership(address payable newOwner) external;

    /**
     * @dev Update the "avatar" value that gets set by default.
     */
    function setAvatarBaseUrl(string calldata url) external;

    /**
     * @dev Update the default TTL value.
     */
    function setDefaultTTL(uint64 newTTL) external;

    /**
     * @dev Pass ownership of a subnode of the contract's root hash to the owner.
     */
    function giveControl(bytes32 nodeId) external;

    /**
     * @dev Rescue ERC20 assets sent directly to this contract.
     */
    function withdrawForeignERC20(address tokenContract) external;

    /**
     * @dev Rescue ERC721 assets sent directly to this contract.
     */
    function withdrawForeignERC721(
        address tokenContract,
        uint256 tokenId
    ) external;

    /**
     * @dev ERC165 support for ENS resolver interface
     * https://docs.ens.domains/contract-developer-guide/writing-a-resolver
     */
    function supportsInterface(bytes4 interfaceID) external pure returns (bool);

    /**
     * @dev For a given ENS Node ID, return the Ethereum address it points to.
     * EIP137 core functionality
     */
    function addr(bytes32 nodeID) external view returns (address);

    /**
     * @dev For a given ENS Node ID, return an address on a different blockchain it points to.
     * EIP2304 functionality
     */
    function addr(
        bytes32 nodeID,
        uint256 coinType
    ) external view returns (bytes memory);

    /**
     * @dev For a given ENS Node ID, set it to point to an address on a different blockchain.
     * EIP2304 functionality
     */
    function setAddr(
        bytes32 nodeID,
        uint256 coinType,
        bytes calldata newAddr
    ) external;

    /**
     * @dev For a given MoonCat rescue order, set the subdomains associated with it to point to an address on a different blockchain.
     */
    function setAddr(
        uint256 rescueOrder,
        uint256 coinType,
        bytes calldata newAddr
    ) external;

    /**
     * @dev For a given ENS Node ID, return the value associated with a given text key.
     * If the key is "avatar", and the matching value is not explicitly set, a url pointing to the MoonCat's image is returned
     * EIP634 functionality
     */
    function text(
        bytes32 nodeID,
        string calldata key
    ) external view returns (string memory);

    /**
     * @dev Update a text record for a specific subdomain.
     * EIP634 functionality
     */
    function setText(
        bytes32 nodeID,
        string calldata key,
        string calldata value
    ) external;

    /**
     * @dev Update a text record for subdomains owned by a specific MoonCat rescue order.
     */
    function setText(
        uint256 rescueOrder,
        string calldata key,
        string calldata value
    ) external;

    /**
     * @dev Get the "content hash" of a given subdomain.
     * EIP1577 functionality
     */
    function contenthash(bytes32 nodeID) external view returns (bytes memory);

    /**
     * @dev Update the "content hash" of a given subdomain.
     * EIP1577 functionality
     */
    function setContenthash(bytes32 nodeID, bytes calldata hash) external;

    /**
     * @dev Update the "content hash" of a given MoonCat's subdomains.
     */
    function setContenthash(uint256 rescueOrder, bytes calldata hash) external;

    /**
     * @dev Set the TTL for a given MoonCat's subdomains.
     */
    function setTTL(uint rescueOrder, uint64 newTTL) external;

    /**
     * @dev Allow calling multiple functions on this contract in one transaction.
     */
    function multicall(
        bytes[] calldata data
    ) external returns (bytes[] memory results);

    /**
     * @dev Reverse lookup for ENS Node ID, to determine the MoonCat rescue order of the MoonCat associated with it.
     */
    function getRescueOrderFromNodeId(
        bytes32 nodeID
    ) external view returns (uint256);

    /**
     * @dev Calculate the "namehash" of a specific domain, using the ENS standard algorithm.
     * The namehash of 'ismymooncat.eth' is 0x204665c32985055ed5daf374d6166861ba8892a3b0849d798c919fffe38a1a15
     * The namehash of 'foo.ismymooncat.eth' is keccak256(0x204665c32985055ed5daf374d6166861ba8892a3b0849d798c919fffe38a1a15, keccak256('foo'))
     */
    function getSubdomainNameHash(
        string memory subdomain
    ) external view returns (bytes32);

    /**
     * @dev Cache a single MoonCat's (identified by Rescue Order) subdomain hashes.
     */
    function mapMoonCat(uint256 rescueOrder) external;

    /**
     * @dev Announce a single MoonCat's (identified by Rescue Order) assigned address.
     */
    function announceMoonCat(uint256 rescueOrder) external;

    /**
     * @dev Has an AddrChanged event been emitted for the current owner of a MoonCat (identified by Rescue Order)?
     */
    function needsAnnouncing(uint256 rescueOrder) external view returns (bool);

    /**
     * @dev Convenience function to iterate through all MoonCats owned by an address to check if they need announcing.
     */
    function needsAnnouncing(
        address moonCatOwner
    ) external view returns (uint256[] memory);

    /**
     * @dev Convenience function to iterate through all MoonCats owned by sender to check if they need announcing.
     */
    function needsAnnouncing() external view returns (uint256[] memory);

    /**
     * @dev Set a manual list of MoonCats (identified by Rescue Order) to announce or cache their subdomain hashes.
     */
    function mapMoonCats(uint256[] memory rescueOrders) external;

    /**
     * @dev Convenience function to iterate through all MoonCats owned by an address and announce or cache their subdomain hashes.
     */
    function mapMoonCats(address moonCatOwner) external;

    /**
     * @dev Convenience function to iterate through all MoonCats owned by the sender and announce or cache their subdomain hashes.
     */
    function mapMoonCats() external;
}
