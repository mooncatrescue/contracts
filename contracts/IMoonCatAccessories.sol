// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

/**
 * @title MoonCat​Accessories
 * @notice external MoonCat Wearables infrastructure/protocols
 * @dev Allows wearable-designers to create accessories for sale and gifting.
 */
interface IMoonCatAccessories {

    /* Events */

    event AccessoryCreated(uint256 accessoryId, address creator, uint256 price, uint16 totalSupply, bytes30 name);
    event AccessoryManagementTransferred(uint256 accessoryId, address newManager);
    event AccessoryPriceChanged(uint256 accessoryId, uint256 price);
    event AccessoryPurchased(uint256 accessoryId, uint256 rescueOrder, uint256 price);
    event AccessoryApplied(uint256 accessoryId, uint256 rescueOrder, uint8 paletteIndex, uint16 zIndex);
    event AccessoryDiscontinued(uint256 accessoryId);

    event EligibleListSet(uint256 accessoryId);
    event EligibleListCleared(uint256 accessoryId);

    /* Structs */

    struct Accessory {            // Accessory Definition
        address payable manager;  // initially creator; payee for sales
        uint8 width;              // image width
        uint8 height;             // image height
        uint8 meta;               // metadata flags [Reserved 3b, Audience 2b, MirrorPlacement 1b, MirrorAccessory 1b, Background 1b]
        uint72 price;             // price at which accessory can be purchased (MAX ~4,722 ETH)
                                  // if set to max value, the accessory is not for sale

        uint16 totalSupply;      // total number of a given accessory that will ever exist; can only be changed by discontinuing the accessory
        uint16 availableSupply;  // number of given accessory still available for sale; decremented on each sale
        bytes28 name;            // unicode name of accessory, can only be set on creation

        bytes8[7] palettes;     // color palettes, each palette is an array of uint8 offsets into the global palette
        bytes2[4] positions;    // offsets for all 4 MoonCat poses, an offset pair of 0xffff indicates the pose is not supported
                                // position order is [standing, sleeping, pouncing, stalking]

        bytes IDAT;            // PNG IDAT chunk data for image reconstruction
    }

    struct OwnedAccessory {   // Accessory owned by an AcclimatedMoonCat
        uint232 accessoryId;  // index into AllAccessories Array
        uint8 paletteIndex;   // index into Accessory.palettes Array
        uint16 zIndex;        // drawing order indicator (lower numbers are closer to MoonCat)
                              // zIndex == 0 indicates the MoonCat is not wearing the accessory
                              // if the accessory meta `Background` bit is 1 the zIndex is interpreted as negative
    }

    struct AccessoryBatchData {   // Used for batch accessory alterations and purchases
        uint256 rescueOrder;
        uint232 ownedIndexOrAccessoryId;
        uint8 paletteIndex;
        uint16 zIndex;
    }

    /* State */

    //bool frozen;
    function frozen () external returns (bool);

    //mapping (uint256 => OwnedAccessory[]) AccessoriesByMoonCat; // AcclimatedMoonCat rescueOrder => Array of AppliedAccessory structs
    function AccessoriesByMoonCat (uint256 rescueOrder) external returns (OwnedAccessory[] calldata);


    //mapping (bytes32 => bool) accessoryHashes; // used to check if the image data for an accessory has already been submitted
    function accessoryHashes (bytes32 idat) external returns (bool);

    //address payable owner;
    function owner () external returns (address);

    //uint256 feeDenominator;
    function feeDenominator () external returns (uint256);

    //uint256 referralDenominator;
    function referralDenonminator () external returns (uint256);

    /**
     * @dev Update the amount of fee taken from each sale
     */
    function setFee (uint256 denominator) external;

    /**
     * @dev Update the amount of referral fee taken from each sale
     */
    function setReferralFee (uint256 denominator) external;

    /**
     * @dev Allow current `owner` to transfer ownership to another address
     */
    function transferOwnership (address payable newOwner) external;

    /**
     * @dev Prevent creating and applying accessories
     */
    function freeze () external;

    /**
     * @dev Enable creating and applying accessories
     */
    function unfreeze () external;

    /**
     * @dev Update the metadata flags for an accessory
     */
    function setMetaByte (uint256 accessoryId, uint8 metabyte) external;

    /**
     * @dev Batch-update metabytes for accessories, by ensuring given bits are on
     */
    function batchOrMetaByte (uint8 value, uint256[] calldata accessoryIds) external;

    /**
     * @dev Batch-update metabytes for accessories, by ensuring given bits are off
     */
    function batchAndMetaByte (uint8 value, uint256[] calldata accessoryIds) external;

    /**
     * @dev Rescue ERC20 assets sent directly to this contract.
     */
    function withdrawForeignERC20 (address tokenContract) external;

    /**
     * @dev Rescue ERC721 assets sent directly to this contract.
     */
    function withdrawForeignERC721 (address tokenContract, uint256 tokenId) external;

    /**
     * @dev Check if a MoonCat is eligible to purchase an accessory
     */
    function isEligible (uint256 rescueOrder, uint256 accessoryId)
        external
        view
        returns (bool);

    /* Creator */

    /**
     * @dev Create an accessory, as the contract owner
     */
    function ownerCreateAccessory (address payable manager, uint8[3] calldata WHM, uint256 priceWei, uint16 totalSupply, bytes28 name, bytes2[4] calldata positions, bytes8[7] calldata initialPalettes, bytes calldata IDAT)
        external
        returns (uint256);

    /**
     * @dev Create an accessory with an eligible list, as the contract owner
     */
    function ownerCreateAccessory (address payable manager, uint8[3] calldata WHM, uint256 priceWei, uint16 totalSupply, bytes28 name, bytes2[4] calldata positions, bytes8[7] calldata initialPalettes, bytes calldata IDAT, bytes32[100] calldata eligibleList)
        external
        returns (uint256);

    /**
     * @dev Create an accessory
     */
    function createAccessory (uint8[3] calldata WHM, uint256 priceWei, uint16 totalSupply, bytes28 name, bytes2[4] calldata positions, bytes8[] calldata palettes, bytes calldata IDAT)
        external
        returns (uint256);

    /**
     * @dev Create an accessory with an eligible list
     */
    function createAccessory (uint8[3] calldata WHM, uint256 priceWei, uint16 totalSupply, bytes28 name, bytes2[4] calldata positions, bytes8[] calldata palettes, bytes calldata IDAT, bytes32[100] calldata eligibleList)
        external
        returns (uint256);

    /**
     * @dev Add a color palette variant to an existing accessory
     */
    function addAccessoryPalette (uint256 accessoryId, bytes8 newPalette) external;

    /**
     * @dev Give ownership of an accessory to someone else
     */
    function transferAccessoryManagement (uint256 accessoryId, address payable newManager) external;

    /**
     * @dev Set accessory to have a new price
     */
    function setAccessoryPrice (uint256 accessoryId, uint256 newPriceWei) external;

    /**
     * @dev Set accessory eligible list
     */
    function setEligibleList (uint256 accessoryId, bytes32[100] calldata eligibleList) external;

    /**
     * @dev Clear accessory eligible list
     */
    function clearEligibleList (uint256 accessoryId) external;

    /**
     * @dev Turns eligible list on or off without setting/clearing
     */
    function toggleEligibleList (uint256 accessoryId, bool active) external;

    /**
     * @dev Add/Remove individual rescueOrders from an eligibleSet
     */
    function editEligibleMoonCats (uint256 accessoryId, bool targetState, uint16[] calldata rescueOrders) external;

    /**
     * @dev Buy an accessory as the manager of that accessory
     */
    function managerApplyAccessory (uint256 rescueOrder, uint256 accessoryId, uint8 paletteIndex, uint16 zIndex)
        external
        returns (uint256);

    /**
     * @dev Remove accessory from the market forever by transferring
     * management to the zero address, setting it as not for sale, and
     * setting the total supply to the current existing quantity.
     */
    function discontinueAccessory (uint256 accessoryId) external;

    /* User */

    /**
     * @dev Buy an accessory that is up for sale by its owner
     *
     * This method is the typical purchase method used by storefronts;
     * it allows the storefront to claim a referral fee for the purchase.
     *
     * Passing a z-index value of zero to this method just purchases the accessory,
     * but does not make it an active part of the MoonCat's appearance.
     */
    function buyAccessory (uint256 rescueOrder, uint256 accessoryId, uint8 paletteIndex, uint16 zIndex, address payable referrer)
        external
        payable
        returns (uint256);

    /**
     * @dev Buy an accessory that is up for sale by its owner
     *
     * This method is a generic fallback method if no referrer address is given for a purchase.
     * Defaults to the owner of the contract to receive the referral fee in this case.
     */
    function buyAccessory (uint256 rescueOrder, uint256 accessoryId, uint8 paletteIndex, uint16 zIndex)
        external
        payable
        returns (uint256);

    /**
     * @dev Buy multiple accessories at once; setting a palette and z-index for each one
     */
    function buyAccessories (AccessoryBatchData[] calldata orders, address payable referrer)
        external
        payable;

    /**
     * @dev Buy multiple accessories at once; setting a palette and z-index for each one (setting the contract owner as the referrer)
     */
    function buyAccessories (AccessoryBatchData[] calldata orders)
        external
        payable;

    /**
     * @dev Change the status of an owned accessory (worn or not, z-index ordering, color palette variant)
     */
    function alterAccessory (uint256 rescueOrder, uint256 ownedAccessoryIndex, uint8 paletteIndex, uint16 zIndex) external;

    /**
    * @dev Change the status of multiple accessories at once
    */
    function alterAccessories (AccessoryBatchData[] calldata alterations) external;

    /* View - Accessories */

    /**
     * @dev How many accessories exist in this contract?
     */
    function totalAccessories ()
        external
        view
        returns (uint256);

    /**
     * @dev Checks if there is an accessory with same IDAT data
     */
    function isAccessoryUnique (bytes calldata IDAT)
        external
        view
        returns (bool);

    /**
     * @dev How many palettes are defined for an accessory?
     */
    function accessoryPaletteCount (uint256 accessoryId)
        external
        view
        returns (uint8);

    /**
     * @dev Fetch a specific palette for a given accessory
     */
    function accessoryPalette (uint256 accessoryId, uint256 paletteIndex)
        external
        view
        returns (bytes8);

    /**
     * @dev Fetch data about a given accessory
     */
    function accessoryInfo (uint256 accessoryId)
        external
        view
        returns (uint16 totalSupply, uint16 availableSupply, bytes28 name, address manager, uint8 metabyte, uint8 availablePalettes, bytes2[4] memory positions, bool availableForPurchase, uint256 price);

    /**
     * @dev Fetch image data about a given accessory
     */
    function accessoryImageData (uint256 accessoryId)
        external
        view
        returns (bytes2[4] memory positions, bytes8[7] memory palettes, uint8 width, uint8 height, uint8 meta, bytes memory IDAT);

    /**
     * @dev Fetch EligibleList for a given accessory
     */
    function accessoryEligibleList (uint256 accessoryId)
        external
        view
        returns (bytes32[100] memory);

    /*  View - Manager */

    /**
     * @dev Which address manages a specific accessory?
     */
    function managerOf (uint256 accessoryId)
        external
        view
        returns (address);

    /**
     * @dev How many accessories does a given address manage?
     */
    function balanceOf (address manager)
        external
        view
        returns (uint256);

    /**
     * @dev Iterate through a given address's managed accessories
     */
    function managedAccessoryByIndex (address manager, uint256 managedAccessoryIndex)
        external
        view
        returns (uint256);

    /*  View - AcclimatedMoonCat */

    /**
     * @dev How many accessories does a given MoonCat own?
     */
    function balanceOf (uint256 rescueOrder)
        external
        view
        returns (uint256);

    /**
     * @dev Iterate through a given MoonCat's accessories
     */
    function ownedAccessoryByIndex (uint256 rescueOrder, uint256 ownedAccessoryIndex)
        external
        view
        returns (OwnedAccessory memory);

    /**
     * @dev Lookup function to see if this MoonCat has already purchased a given accessory
     */
    function doesMoonCatOwnAccessory (uint256 rescueOrder, uint256 accessoryId)
        external
        view
        returns (bool);

}
