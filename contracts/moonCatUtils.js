// @ts-check
const { expect } = require('chai')

const MoonCatRescueArtifact = require('./artifacts/contracts/IMoonCatRescue.sol/IMoonCatRescue.json'),
  OldWrappedMoonCatArtifact = require('./artifacts/contracts/IMoonCatsWrapped.sol/IMoonCatsWrapped.json'),
  AcclimatorArtifact = require('./artifacts/contracts/IMoonCatAcclimator.sol/IMoonCatAcclimator.json'),
  AccessoriesArtifact = require('./artifacts/contracts/IMoonCatAccessories.sol/IMoonCatAccessories.json'),
  MomentsArtifact = require('./artifacts/contracts/IMoonCatMoments.sol/IMoonCatMoments.json'),
  NameServiceArtifact = require('./artifacts/contracts/IMoonCatNameService.sol/IMoonCatNameService.json'),
  LootprintsArtifact = require('./artifacts/contracts/IMoonCatLootprints.sol/IMoonCatLootprints.json'),
  JumpPortArtifact = require('./artifacts/contracts/IJumpPort.sol/IJumpPort.json'),
  MoonCatTraitsArtifact = require('./artifacts/contracts/IMoonCatTraits.sol/IMoonCatTraits.json'),
  MoonCatColorsArtifact = require('./artifacts/contracts/IMoonCatColors.sol/IMoonCatColors.json'),
  MoonCatSVGsArtifact = require('./artifacts/contracts/IMoonCatSVGs.sol/IMoonCatSVGs.json'),
  MoonCatAccessoryImagesArtifact = require('./artifacts/contracts/IMoonCatAccessoryImages.sol/IMoonCatAccessoryImages.json'),
  MoonCatReferenceArtifact = require('./artifacts/contracts/IMoonCatReference.sol/IMoonCatReference.json')

const ZWS = '\u200B'

/** @type {Record<string, { address: `0x${string}`, abi: import('@ethersproject/abi').JsonFragment[], name: string, deployed: `0x${string}`, description: string }>} */
const contracts = {
  rescue: {
    address: '0x60cd862c9C687A9dE49aecdC3A99b74A4fc54aB6',
    abi: MoonCatRescueArtifact.abi,
    name: `MoonCat${ZWS}Rescue`,
    deployed:
      '0x79d48c41b99f0ac8f735dbf4d048165542576862df2b05a80be9a4dbe233a623',
    description: `
The original contract that started this project off! It is an [ERC20](https://eips.ethereum.org/EIPS/eip-20)-like contract that creates the possibility of a collection of 25,600 MoonCat tokens, each with a unique five-byte hexadecimal identifier to be "rescued" into the blockchain. It was created before the [ERC721](https://eips.ethereum.org/EIPS/eip-721) standard existed, so the function names are different than that standard. Some of the Genesis MoonCats did not get released as originally planned, and the community opted to have them remain on the moon, so the total collection size is 25,440 MoonCats.

This contract has a built-in marketplace where those who already own a MoonCat can create \`AdoptionOffers\` to list their MoonCat as adoptable (either by anyone or only by a specific address), and those who want to adopt a MoonCat can create an \`AdoptionRequest\` for a specific MoonCat.

Each MoonCat's hexadecimal identifier defines the MoonCat's pose, facial expression, fur pattern, and color. This identifier was permanently stored in this contract when that MoonCat was rescued, and cannot be changed. Each MoonCat can have a name given to them by their owner, but can only be named once. Once a MoonCat is named, the name is permanently stored in this contract, and cannot be changed.
`,
  },
  oldWrapped: {
    address: '0x7C40c393DC0f283F318791d746d894DdD3693572',
    abi: OldWrappedMoonCatArtifact.abi,
    name: `Old MoonCat Wrapper`,
    deployed:
      '0xb27719066844420204ba6b5ce4ce2d2fdae1262270a8b1a211e96cda16636818',
    description: `
A "wrapping" contract that was created in the flurry that happened after the March 2021 "rediscovery". It converts MoonCats from the original contract into [ERC721](https://eips.ethereum.org/EIPS/eip-721) tokens, but does not preserve other metadata about the MoonCats. This contract enables MoonCats to be seen by modern wallet and portfolio tools, but is considered deprecated in favor of the Acclimator wrapper contract.
`,
  },
  acclimator: {
    address: '0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69',
    abi: AcclimatorArtifact.abi,
    name: `MoonCat${ZWS}Acclimator`,
    deployed:
      '0x5585c88321058bde24b5ed025b51323731e2186836d3d5bdf66e7aed7ab63461',
    description: `
A "wrapping" contract that takes a MoonCat from the original contract, and converts it to an [ERC721](https://eips.ethereum.org/EIPS/eip-721)- and [ERC998](https://eips.ethereum.org/EIPS/eip-998)-compatible token. The web interface for interacting with this contract is [The Acclimator](https://chainstation.mooncatrescue.com/acclimator).

The ERC998 functionality is a "top down, ERC721" implementation, which enables individual MoonCats to own other ERC721 tokens. Other ERC721 tokens that get sent to this smart contract using the \`safeTransferFrom\` function should specify in the \`data\` parameter the Rescue Order of the MoonCat that should receive the token.
`,
  },
  accessories: {
    address: '0x8d33303023723dE93b213da4EB53bE890e747C63',
    abi: AccessoriesArtifact.abi,
    name: `MoonCat${ZWS}Accessories`,
    deployed:
      '0x21b831be284a48d6f2d72b84e94cfdd8123ec4b28779b098e8bc8caa7f02755a',
    description: `
On-chain visual modifications for MoonCats that can be purchased by their owners. The web interface for interacting with this contract is [The Boutique](https://chainstation.mooncatrescue.com/accessories).

Purchasing an accessory (\`buyAccessory\` or \`buyAccessories\` functions) is like "unlocking a skin" for your MoonCat. Once purchased, the accessory becomes available to that MoonCat forever ("part of their wardrobe"). The owner of a MoonCat can choose which of the MoonCat's accessories are visible via the \`alterAccessory\` or \`alterAccessories\` functions.

The data for an accessory's visual is stored as a PNG-8 image, though split up into multiple pieces to make it easier to swap in different palettes. This allows each accessory to have multiple color variants without needing to store the image data multiple times, and allows for accessories to have colors that adapt to the MoonCat that wears them.
`,
  },
  mcns: {
    address: '0x2eCcDA7C84837Da55ce34A36a5AD2B936479E76e',
    abi: NameServiceArtifact.abi,
    name: `MoonCatNameService (MCNS)`,
    deployed:
      '0x21b831be284a48d6f2d72b84e94cfdd8123ec4b28779b098e8bc8caa7f02755a',
    description: `
A contract that adheres to the [Ethereum Name Service (ENS)](https://ens.domains/) [Resolver interface](https://docs.ens.domains/resolvers/quickstart), but has custom logic to allow all MoonCat owners to claim subdomains that matches their MoonCat's hexadecimal identifier and rescue order. The web interface for interacting with this contract is the [MoonCatNameService (MCNS)](https://chainstation.mooncatrescue.com/mcns).
`,
  },
  moments: {
    address: '0x367721b332F4697d440EBBe6780262411Fd03409',
    abi: MomentsArtifact.abi,
    name: `MoonCat${ZWS}Moments`,
    deployed:
      '0xe13e6e711af7ea6a2ea61c23f4b03be5c8957d05ef19fd190eb9ca65456503b0',
    description: `
An [ERC721](https://eips.ethereum.org/EIPS/eip-721) token collection that represents moments in time for the MoonCat${ZWS}Rescue community. They are photographic souvenirs for the MoonCats that participated in each Moment. The web interface for interacting with this contract is [on ChainStation](https://chainstation.mooncatrescue.com/moments).

Each token in this collection shares a visual with many others (all tokens from the same Moment have the same visual). However this is done not with different "classes" (like an ERC1155 collection), but rather sequential ranges of token IDs are used for each Moment. This collection is expected to grow as additional Moments are created.
`,
  },
  lootprints: {
    address: '0x1e9385eE28c5C7d33F3472f732Fb08CE3ceBce1F',
    abi: LootprintsArtifact.abi,
    name: `lootprints (for MoonCats)`,
    deployed:
      '0xce2a9c92f69b5276ffbe3ccbe453f289a6f619bb623b3ded3ebfa1239ac9f77e',
    description: `
An [ERC721](https://eips.ethereum.org/EIPS/eip-721) token collection that represents plans for building MoonCat-styled space ships. The visuals for these tokens come in all the colors that the original MoonCat collection does, so they're not just "blueprints", but are instead "lootprints".

Inspired by the [Loot](https://loot.xyz/) project, these tokens show their stats in a simplified way as SVGs, output directly by the smart contract.
`,
  },
  jumpPort: {
    address: '0xF4d150F3D03Fa1912aad168050694f0fA0e44532',
    abi: JumpPortArtifact.abi,
    name: `The JumpPort`,
    deployed:
      '0x591059774a676071a188fd8805e066b9f53c95c1cdeb76f1a4347c011b35f9b4',
    description: `
A universal staking contract, designed to be a programmable hub for MoonCats to participate in other projects that require them be held in place for specific times. But this contract is universal; any [ERC721](https://eips.ethereum.org/EIPS/eip-721) collection can use the JumpPort as well!

The JumpPort is different than other "bridge" contracts in that it is designed for tokens to interact with multiple things at once. In lore this is described as "projecting" them into other blockchains, rather than moving/bridging them there. Therefore, if a project wishes to integrate with the JumpPort, it is vital that tokens that are "projected" to other chains are not sold to a new owner while there (it should not be treated as "real" while on the other chain).

The JumpPort operates by having additional smart contracts that are designated as "Portals", which implement logic to allow tokens to be removed from the JumpPort or not. A single token in the JumpPort can interact with multiple portals, but to withdraw the token from the JumpPort, it must be unlocked by all the portals it has interacted with.
`,
  },
  moonCatTraits: {
    address: '0x9330BbfBa0C8FdAf0D93717E4405a410a6103cC2',
    abi: MoonCatTraitsArtifact.abi,
    name: `MoonCat${ZWS}Traits`,
    deployed:
      '0xc6a79b35660dbc06cafce69f9d544c44c911004d30d0f0b269c6f82378cec0f7',
    description: `
Part of the suite of on-chain metadata contracts for MoonCats. A smart-contract implementation of the original JavaScript parser. This contract can take any MoonCat's hexadecimal ID (its "DNA") and parse out the trait information for it. It's useful for other smaart contracts that wish to be able to parse MoonCat identifiers, and turn them into human-friendly labels.
`,
  },
  moonCatColors: {
    address: '0x2fd7E0c38243eA15700F45cfc38A7a7f66df1deC',
    abi: MoonCatColorsArtifact.abi,
    name: `MoonCat${ZWS}Colors`,
    deployed:
      '0xd2c70642c15d5e068c331459f5d12e5cd2926729cfd3c010d9a1e00df69a4410',
    description: `
Part of the suite of on-chain metadata contracts. This contract parses a MoonCat's hexadecimal ID and returns the calculated color palette for that MoonCat. This is useful for other smart contracts that wish to be able to parse MoonCat identifiers, and determine what colors should be used to render them. The canonical parser for MoonCat data was created in JavaScript, and due to the differences in how floating point numbers are represented between JavaScript and Solidity, this contract has baked in a static list of which MoonCats have different outcomes between JavaScript and Solidity, and does output the original JavaScript values for those MoonCats.
`,
  },
  moonCatSvgs: {
    address: '0xB39C61fe6281324A23e079464f7E697F8Ba6968f',
    abi: MoonCatSVGsArtifact.abi,
    name: `MoonCat${ZWS}SVGs`,
    deployed:
      '0xbabdec945b57fd0b1a009aa0b98e84b310d91c91d3033e7ce9f0881dcfde2954',
    description: `
Part of the suite of on-chain metadata contracts. This contract parses a MoonCat's hexadecimal ID and returns a visual of that MoonCat in SVG format. It relies on the MoonCat${ZWS}Colors contract to determine the colors used in the SVG.
`,
  },
  moonCatAccessoryImages: {
    address: '0x91CF36c92fEb5c11D3F5fe3e8b9e212f7472Ec14',
    abi: MoonCatAccessoryImagesArtifact.abi,
    name: `MoonCat${ZWS}AccessoryImages`,
    deployed:
      '0xf1b559d8772f4c8d831a723826fd6bb045be8d5740a7fe3255a581300a90c147',
    description: `
Part of the suite of on-chain metadata contracts. This contract parses a MoonCat's hexadecimal ID and returns a visual of that MoonCat in SVG format with accessories that MoonCat owns. This contract uses the MoonCat${ZWS}SVGs contract to determine the base visual of the MoonCat, and then uses the MoonCat${ZWS}Accessories contract to determine the accessories that the MoonCat owns and overlays them.

The MoonCat is drawn with native SVG shapes (rects and polygons), and the accessories are embedded into this as PNG images in the SVG file.
`,
  },
  reference: {
    address: '0x0B78C64bCE6d6d4447e58b09E53F3621f44A2a48',
    abi: MoonCatReferenceArtifact.abi,
    name: `MoonCat${ZWS}Reference`,
    deployed:
      '0x4a3b15b2b44f896526ef5e158309a1fefee7db51fdcb58c5ecc1232833bee21a',
    description: `
A contract for storing on-chain documentation in a human-friendly way. It provides on-chain links between the MoonCat${ZWS}Rescue-related contracts, and links to longer descriptions of each of those smart contracts.
`,
  },
}

/**
 * Generate blocks on the Hardhat internal fork until the base gas fee is below a given amount
 *
 * This is helpful if the snapshot block being forked was at a time of high gas, this can settle it down before testing
 * @param {number | import('ethers').BigNumber} targetGas
 * @returns {Promise<undefined>}
 */
async function mineUntilGasTarget(targetGas) {
  const { ethers, network } = require('hardhat')
  for (let i = 1; i <= 50; i++) {
    let blockData = await ethers.provider.getBlock('latest')
    if (
      blockData.baseFeePerGas == null ||
      blockData.baseFeePerGas.lte(targetGas)
    )
      return
    //console.log(blockData.number.toLocaleString(), ethers.utils.formatUnits(blockData.baseFeePerGas, 'gwei'));
    await network.provider.request({
      method: 'evm_mine',
      params: [],
    })
  }
}

/**
 * Send some MoonCats to the default connected accounts
 * @param {number} startIndex The rescue order to start dispersing from
 * @param {number} maxIndex The last rescue order to disperse
 * @param {number} [numAccounts = 10]
 * @returns {Promise<undefined>}
 */
async function disperseMoonCats(startIndex, maxIndex, numAccounts = 10) {
  const { ethers } = require('hardhat')
  const accounts = await ethers.getSigners()
  await mineUntilGasTarget(ethers.utils.parseUnits('5', 'gwei'))

  for (let i = startIndex; i <= maxIndex; i++) {
    let testAccountTargetIndex = i % numAccounts
    let newOwner = accounts[testAccountTargetIndex].address
    await giveMoonCatAway(i, newOwner, accounts[15])
  }
}

/**
 * Send some lootprints to the default connected accounts
 * @param {number} startIndex The token ID to start dispersing from
 * @param {number} maxIndex The last token ID to disperse
 * @param {number} [numAccounts = 5]
 * @returns {Promise<undefined>}
 */
async function disperseLootprints(startIndex, maxIndex, numAccounts = 5) {
  const { ethers } = require('hardhat')
  const accounts = await ethers.getSigners()
  const LOOTPRINTS = new ethers.Contract(
    contracts.lootprint.address,
    contracts.lootprint.abi,
    accounts[0]
  )
  await mineUntilGasTarget(ethers.utils.parseUnits('5', 'gwei'))

  let currentIndex = 0
  for (let i = startIndex; i <= maxIndex; i++) {
    let details = await LOOTPRINTS.getDetails(i)
    if (details.status != 3) {
      //console.log(`Target lootprint ${i} doesn't exist...`);
      continue
    }
    let testAccountTargetIndex = currentIndex % numAccounts
    let newOwner = accounts[testAccountTargetIndex].address
    await giveLootprintAway(i, newOwner, accounts[15])
    currentIndex++
  }
}

/**
 * Send an individual MoonCat to another address
 * @param {number} rescueOrder MoonCat to transfer
 * @param {string} newOwner Address to transfer the MoonCat to
 * @param {import('ethers').Signer} moneybag Account to pull ETH from, if the current owner doesn't have funds for a transfer
 * @returns {Promise<undefined>}
 */
async function giveMoonCatAway(rescueOrder, newOwner, moneybag) {
  const { ethers, network } = require('hardhat')
  const accounts = await ethers.getSigners()
  const mcr = new ethers.Contract(
      contracts.rescue.address,
      contracts.rescue.abi,
      accounts[0]
    ),
    oldWrapper = new ethers.Contract(
      contracts.oldWrapped.address,
      contracts.oldWrapped.abi,
      accounts[0]
    ),
    acclimator = new ethers.Contract(
      contracts.acclimator.address,
      contracts.acclimator.abi,
      accounts[0]
    )

  const transferCost = ethers.utils.parseEther('0.3')
  let catId = await mcr.rescueOrder(rescueOrder)
  let catData = await mcr.getCatDetails(catId)
  let owner = catData.owner

  async function prepOldOwner(owner) {
    await network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [owner],
    })
    let signer = await ethers.provider.getSigner(owner)

    // Check if they have enough for the transfer
    let currentBalance = await signer.getBalance()
    if (currentBalance.lt(transferCost)) {
      await moneybag.sendTransaction({ to: owner, value: transferCost })
    }
    return signer
  }

  if (owner == contracts.oldWrapped.address) {
    // This is an old-wrapped MoonCat; acclimate-and-transfer it to the new owner
    let wrappedId = await oldWrapper._catIDToTokenID(catId)
    owner = await oldWrapper.ownerOf(wrappedId)

    console.log(
      `Current owner ${owner} is giving wrapped ${wrappedId} ${catId} (#${rescueOrder}) => ${newOwner}`
    )
    let signer = await prepOldOwner(owner)

    let data = numToBytes32(rescueOrder) + newOwner.substring(2)
    await oldWrapper
      .connect(signer)
      ['safeTransferFrom(address,address,uint256,bytes)'](
        owner,
        contracts.acclimator.address,
        wrappedId,
        data
      )
  } else if (owner == contracts.acclimator.address) {
    // Already acclimated
    owner = await acclimator.ownerOf(rescueOrder)
    if (owner == newOwner) {
      console.log(
        `Target owner ${newOwner} already owns ${catId} (#${rescueOrder})...`
      )
      return
    }

    // Send to new owner
    let signer = await prepOldOwner(owner)
    console.log(
      `Current owner ${owner} is giving acclimated ${catId} (#${rescueOrder}) => ${newOwner}`
    )
    await acclimator
      .connect(signer)
      ['safeTransferFrom(address,address,uint256)'](
        owner,
        newOwner,
        rescueOrder
      )
  } else {
    // Original MoonCat; wrap and transfer it to new owner
    console.log(
      `Current owner ${owner} is giving ${catId} (#${rescueOrder}) => ${newOwner}`
    )
    let signer = await prepOldOwner(owner)
    await mcr
      .connect(signer)
      .makeAdoptionOfferToAddress(catId, 0, contracts.acclimator.address)
    await acclimator.connect(signer).wrap(rescueOrder)
    await acclimator
      .connect(signer)
      ['safeTransferFrom(address,address,uint256)'](
        owner,
        newOwner,
        rescueOrder
      )
  }

  // Cleanup
  await network.provider.request({
    method: 'hardhat_stopImpersonatingAccount',
    params: [owner],
  })
}

/**
 * Send an individual lootprint to another address
 * @param {number} tokenId lootprint to transfer
 * @param {string} newOwner Address to transfer the lootprint to
 * @param {import('ethers').Signer} moneybag Account to pull ETH from, if the current owner doesn't have funds for a transfer
 * @returns {Promise<undefined>}
 */
async function giveLootprintAway(tokenId, newOwner, moneybag) {
  const { ethers, network } = require('hardhat')
  const accounts = await ethers.getSigners()
  const LOOTPRINTS = new ethers.Contract(
    contracts.lootprints.address,
    contracts.lootprints.abi,
    accounts[0]
  )

  const transferCost = ethers.utils.parseEther('0.3')
  let owner = await LOOTPRINTS.ownerOf(tokenId)

  async function prepOldOwner(owner) {
    await network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [owner],
    })
    let signer = await ethers.provider.getSigner(owner)

    // Check if they have enough for the transfer
    let currentBalance = await signer.getBalance()
    if (currentBalance.lt(transferCost)) {
      await moneybag.sendTransaction({ to: owner, value: transferCost })
    }
    return signer
  }

  if (owner == newOwner) {
    console.log(`Target owner ${newOwner} already owns ${tokenId}...`)
    return
  }

  // Send to new owner
  let signer = await prepOldOwner(owner)
  console.log(
    `Current owner ${owner} is giving lootprint ${tokenId} => ${newOwner}`
  )
  await LOOTPRINTS.connect(signer)['safeTransferFrom(address,address,uint256)'](
    owner,
    newOwner,
    tokenId
  )

  // Cleanup
  await network.provider.request({
    method: 'hardhat_stopImpersonatingAccount',
    params: [owner],
  })
}

/**
 * Chai handler for custom contract errors
 *
 * The ability to parse custom errors to get their arguments is being added in Waffle 4.0:
 * https://ethereum-waffle.readthedocs.io/en/latest/migration-guides.html#custom-errors
 * Until that's added, this custom parser can be used to check the properties
 * @param {Promise<import('ethers').providers.TransactionReceipt>} tx
 * @param {string} role
 * @param {string} address
 * @returns {Promise<undefined>}
 */
async function expectPermissionError(tx, role, address) {
  try {
    await tx
    expect.fail('Expected transaction to revert, but it succeeded')
  } catch (err) {
    let expectedMessage
    if (err.message.indexOf('with custom error') > 0) {
      // Custom error was recognized
      expectedMessage = `VM Exception while processing transaction: reverted with custom error 'MissingRole("${role}", "${address}")'`
    } else {
      // Raw error message
      let paddedRole = (
        '0000000000000000000000000000000000000000000000000000000000000000' +
        role.substring(2).toLowerCase()
      ).slice(-64)
      let paddedAddr = (
        '0000000000000000000000000000000000000000000000000000000000000000' +
        address.substring(2).toLowerCase()
      ).slice(-64)
      expectedMessage = `VM Exception while processing transaction: reverted with an unrecognized custom error (return data: 0x75000dc0${paddedRole}${paddedAddr})`
    }
    expect(err.message).to.equal(expectedMessage, 'Missing Role error thrown')
  }
}

/**
 * Convert a number to a hex-encoded 32-byte string
 *
 * @param {number} num
 * @returns {string}
 */
function numToBytes32(num) {
  return '0x' + num.toString(16).padStart(64, '0')
}

module.exports = {
  contracts,

  disperseMoonCats,
  giveMoonCatAway,
  disperseLootprints,
  giveLootprintAway,

  mineUntilGasTarget,
  expectPermissionError,
  numToBytes32,
}
