// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

/**
 * @title MoonCatSVGs
 * @notice On Chain MoonCat Image Generation
 * @dev Builds SVGs of MoonCat Images
 */
interface IMoonCatSVGs {

    //address payable public owner;
    function owner() external view returns (address);

    //uint8[16] public CatBox;
    function CatBox(uint256) external view returns (uint8);

    //string constant public Face;
    function Face() external view returns (string memory);

    //string[4] public Border;
    function Border(uint256) external view returns (string memory);

    //string[4] public Coat;
    function Coat(uint256) external view returns (string memory);

    //string[4] public Tummy;
    function Tummy(uint256) external view returns (string memory);

    //uint8[] public Eyes;
    function Eyes(uint256) external view returns (uint8);

    //uint8[] public Whiskers;
    function Whiskers(uint256) external view returns (uint8);

    //uint8[] public Skin;
    function Skin(uint256) external view returns (uint8);

    //uint8[882] public Patterns;
    function Patterns(uint256) external view returns (uint8);


    /**
     * @dev Wrap a chunk of SVG objects with a group that flips their appearance horizontally.
     */
    function flip (bytes memory svgData) external pure returns (bytes memory);

    /**
     * @dev Transform a set of coordinate points into a collection of SVG rectangles.
     */
    function pixelGroup (uint8[] memory data, uint8 index) external pure returns (bytes memory);

    /**
     * @dev For a given MoonCat pose and pattern ID, return a collection of SVG rectangles drawing that pattern.
     */
    function getPattern (uint8 pose, uint8 pattern) external view returns (bytes memory);

    /**
     * @dev Wrap a collection of SVG rectangle "pixels" in a group to create a colored glow around them.
     */
    function glowGroup (bytes memory pixels, uint8 r, uint8 g, uint8 b) external pure returns (bytes memory);

    /**
     * @dev Given specific MoonCat trait information, assemble the main visual SVG objects to represent a MoonCat with those traits.
     */
    function getPixelData (uint8 facing, uint8 expression, uint8 pose, uint8 pattern, uint8[24] memory colors)
        external
        view
        returns (bytes memory);

    /**
     * @dev Construct SVG header/wrapper tag for a given set of canvas dimensions.
     */
    function svgTag (uint8 x, uint8 y, uint8 w, uint8 h) external pure returns (bytes memory);

    /**
     * @dev Convert a MoonCat facing and pose trait information into an SVG viewBox definition to set that canvas size.
     */
    function boundingBox (uint8 facing, uint8 pose) external view returns (uint8 x, uint8 y, uint8 width, uint8 height);

    /**
     * @dev For a given MoonCat hex ID, create an SVG of their appearance, specifying glowing or not.
     */
    function imageOf (bytes5 catId, bool glow) external view returns (string memory);

    /**
     * @dev For a given MoonCat hex ID, create an SVG of their appearance, glowing if they're Acclimated.
     */
    function imageOf (bytes5 catId) external view returns (string memory);

    /**
     * @dev For a given MoonCat rescue order, create an SVG of their appearance, specifying glowing or not.
     */
    function imageOf (uint256 rescueOrder, bool glow) external view returns (string memory);

    /**
     * @dev For a given MoonCat rescue order, create an SVG of their appearance, glowing if they're Acclimated.
     */
    function imageOf (uint256 rescueOrder) external view returns (string memory);

    /**
     * @dev Convert an integer/numeric value into a string of that number's decimal value.
     */
    function uint2str (uint value) external pure returns (string memory);

    /* General */

    /**
     * @dev Get documentation about this contract.
     */
    function doc() external view returns (string memory name, string memory description, string memory details);

    /**
     * @dev Allow current `owner` to transfer ownership to another address.
     */
    function transferOwnership (address payable newOwner) external;

    /**
     * @dev Update the location of the Reference Contract.
     */
    function setReferenceContract (address referenceContract) external;

    /**
     * @dev Rescue ERC20 assets sent directly to this contract.
     */
    function withdrawForeignERC20 (address tokenContract) external;

    /**
     * @dev Rescue ERC721 assets sent directly to this contract.
     */
    function withdrawForeignERC721 (address tokenContract, uint256 tokenId) external;
}