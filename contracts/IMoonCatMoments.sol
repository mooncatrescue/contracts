// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

interface IERC165 {
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}

interface IERC721 is IERC165 {
    event Transfer(
        address indexed from,
        address indexed to,
        uint256 indexed tokenId
    );
    event Approval(
        address indexed owner,
        address indexed approved,
        uint256 indexed tokenId
    );
    event ApprovalForAll(
        address indexed owner,
        address indexed operator,
        bool approved
    );
    function balanceOf(address owner) external view returns (uint256 balance);
    function ownerOf(uint256 tokenId) external view returns (address owner);
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId
    ) external;
    function transferFrom(address from, address to, uint256 tokenId) external;
    function approve(address to, uint256 tokenId) external;
    function getApproved(
        uint256 tokenId
    ) external view returns (address operator);
    function setApprovalForAll(address operator, bool _approved) external;
    function isApprovedForAll(
        address owner,
        address operator
    ) external view returns (bool);
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId,
        bytes calldata data
    ) external;
}

interface IERC721Metadata is IERC721 {
    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function tokenURI(uint256 tokenId) external view returns (string memory);
}

interface IERC721Enumerable is IERC721 {
    function totalSupply() external view returns (uint256);
    function tokenOfOwnerByIndex(
        address owner,
        uint256 index
    ) external view returns (uint256 tokenId);
    function tokenByIndex(uint256 index) external view returns (uint256);
}

/**
 * @title MoonCat​Moments
 * @notice NFTs of group photos MoonCats take together, commemorating special events
 * @dev ERC721-compliant tokens, minted into existence as owned by the MoonCats themselves (ERC998 functionality)
 */
interface IMoonCatMoments is IERC721Enumerable, IERC721Metadata {
    struct Moment {
        uint16 momentId;
        uint16 startingTokenId;
        uint16 issuance;
        uint16 unclaimed;
        string tokenURI;
        bytes32[100] claimable;
    }

    //address public constant moonCatAcclimatorContract;
    function moonCatAcclimatorContract() external view returns (address);

    //address public contractOwner;
    function contractOwner() external view returns (address);

    //bool public paused;
    function paused() external view returns (bool);

    //uint256 public acclimatorBalance;
    function acclimatorBalance() external view returns (uint256);

    //uint16 public totalMoments;
    function totalMoments() external view returns (uint16);

    //mapping(uint256 => Moment) public Moments;
    function Moments(uint256) external view returns (Moment memory);

    /**
     * @dev Claim a Moment token that a given MoonCat is eligible for
     */
    function claim(
        uint256 momentId,
        uint256 rescueOrder
    ) external returns (uint256);

    /**
     * @dev Claim several Moments that multiple MoonCats are eligible for
     */
    function batchClaim(
        uint256[] calldata momentIds,
        uint256[] calldata rescueOrders
    ) external;

    /**
     * @dev Claim a Moment that multiple MoonCats are eligible for
     */
    function batchClaim(
        uint256 momentId,
        uint256[] calldata rescueOrders
    ) external;

    /**
     * @dev Claim several Moments that a MoonCat is eligible for
     */
    function batchClaim(
        uint256[] calldata momentIds,
        uint256 rescueOrder
    ) external;

    /**
     * @dev For a given MoonCat, claim any pending Moment they're eligible for
     */
    function batchClaim(uint256 rescueOrder) external;

    /**
     * @dev For multiple MoonCats, claim any pending Moments they're eligible for
     */
    function batchClaim(uint256[] calldata rescueOrders) external;

    /**
     * @dev For a given MoonCat, fetch a list of Moment IDs that MoonCat is eligible to claim
     */
    function listClaimableMoments(
        uint256 rescueOrder
    ) external view returns (uint16[] memory);

    /* Owner Functions */

    /**
     * @dev Allow current `owner` to transfer ownership to another address
     */
    function transferOwnership(address newOwner) external;

    /**
     * @dev Create a new Moment, and deliver the tokens directly to the indicated MoonCats
     */
    function mint(string calldata URI, uint16[] calldata rescueOrders) external;

    /**
     * @dev Create a new Moment, to be made available to MoonCat owners to claim
     */
    function mintClaimable(
        string calldata URI,
        uint16[] calldata rescueOrders
    ) external;

    /**
     * @dev Check and see if a given MoonCat is able to claim a given Moment
     */
    function isClaimable(
        uint256 momentId,
        uint256 rescueOrder
    ) external view returns (bool);

    /**
     * @dev Given a list of MoonCat rescue orders, check a single Moment identifier and return a list of which of those MoonCats can claim it
     */
    function isClaimable(
        uint256 momentId,
        uint256[] calldata rescueOrders
    ) external view returns (bool[] memory);

    /**
     * @dev Update the metadata location for a given Moment
     */
    function setMomentURI(uint256 momentId, string calldata URI) external;

    /**
     * @dev Prevent claiming of Moments
     */
    function paws() external;

    /**
     * @dev Enable claiming of Moments
     */
    function unpaws() external;

    /* Metadata */

    /**
     * @dev For a given Moment token identifier, determine which Moment group it is a part of
     */
    function getMomentId(uint256 tokenId) external view returns (uint256);

    /**
     * @dev See {IERC721Metadata-tokenURI}.
     */
    function tokenURI(uint256 tokenId) external view returns (string memory);

    /**
     * @dev For a given Moment ID, fetch the metadata URI for that moment
     */
    function tokenURIByMomentId(
        uint256 momentId
    ) external view returns (string memory);

    /**
     * @dev Does a Moment token with the specified identifier exist?
     * Because some Moments are claimable, and don't come into existence until claimed, there will be gaps in the ID sequence.
     * This function gives a way to determine easily if a given identifier exists at all, before attempting further actions on it
     */
    function tokenExists(uint256 tokenId) external view returns (bool);

    /* Rescuers */

    /**
     * @dev Rescue ERC20 assets sent directly to this contract.
     */
    function withdrawForeignERC20(address tokenContract) external;

    /**
     * @dev Rescue ERC721 assets sent directly to this contract.
     */
    function withdrawForeignERC721(
        address tokenContract,
        uint256 tokenId
    ) external;
}
