// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

/** Color Names by Full Palette Index **

  0 - Transparent Background
  1 - White
  2 - Pale Grey
  3 - Light Grey
  4 - Grey
  5 - Dark Grey
  6 - Deep Grey
  7 - Black
  8 - Light Red
  9 - Red
 10 - Dark Red
 11 - Light Orange
 12 - Orange
 13 - Dark Orange
 14 - Light Gold
 15 - Gold
 16 - Dark Gold
 17 - Light Yellow
 18 - Yellow
 19 - Dark Yellow
 20 - Light Chartreuse
 21 - Chartreuse
 22 - Dark Chartreuse
 23 - Light Green
 24 - Green
 25 - Dark Green
 26 - Light Teal
 27 - Teal
 28 - Dark Teal
 29 - Light Cyan
 30 - Cyan
 31 - Dark Cyan
 32 - Light Sky Blue
 33 - Sky Blue
 34 - Dark Sky Blue
 35 - Light Blue
 36 - Blue
 37 - Dark Blue
 38 - Light Indigo
 39 - Indigo
 40 - Dark Indigo
 41 - Light Purple
 42 - Purple
 43 - Dark Purple
 44 - Light Violet
 45 - Violet
 46 - Dark Violet
 47 - Light Pink
 48 - Pink
 49 - Dark Pink
 50 - Deep Red
 51 - Deep Yellow
 52 - Deep Green
 53 - Deep Teal
 54 - Deep Blue
 55 - Deep Purple
 56 - Deep Pink
 57 - Pale Red
 58 - Pale Yellow
 59 - Pale Green
 60 - Pale Teal
 61 - Pale Blue
 62 - Pale Purple
 63 - Pale Pink
 64 - Umber
 65 - Mocha
 66 - Cinnamon
 67 - Brown
 68 - Peanut
 69 - Tortilla
 70 - Beige
 71 - White Glass
 72 - Pale Grey Glass
 73 - Light Grey Glass
 74 - Grey Glass
 75 - Dark Grey Glass
 76 - Deep Grey Glass
 77 - Black Glass
 78 - Vibrant Red Smoked Glass
 79 - Dull Red Smoked Glass
 80 - Vibrant Yellow Smoked Glass
 81 - Dull Yellow Smoked Glass
 82 - Vibrant Green Smoked Glass
 83 - Dull Green Smoked Glass
 84 - Vibrant Teal Smoked Glass
 85 - Dull Teal Smoked Glass
 86 - Vibrant Blue Smoked Glass
 87 - Dull Blue Smoked Glass
 88 - Vibrant Purple Smoked Glass
 89 - Dull Purple Smoked Glass
 90 - Vibrant Pink Smoked Glass
 91 - Dull Pink Smoked Glass
 92 - Vibrant Red Stained Glass
 93 - Dull Red Stained Glass
 94 - Vibrant Yellow Stained Glass
 95 - Dull Yellow Stained Glass
 96 - Vibrant Green Stained Glass
 97 - Dull Green Stained Glass
 98 - Vibrant Teal Stained Glass
 99 - Dull Teal Stained Glass
100 - Vibrant Blue Stained Glass
101 - Dull Blue Stained Glass
102 - Vibrant Purple Stained Glass
103 - Dull Purple Stained Glass
104 - Vibrant Pink Stained Glass
105 - Dull Pink Stained Glass
106 - Red Tinted Glass
107 - Yellow Tinted Glass
108 - Green Tinted Glass
109 - Teal Tinted Glass
110 - Blue Tinted Glass
111 - Purple Tinted Glass
112 - Pink Tinted Glass
113 - MoonCat Glow Color <- accessoryColorsOf indexes start here
114 - MoonCat Border (glows)
115 - MoonCat Pattern
116 - MoonCat Coat
117 - MoonCat Belly/Whiskers
118 - MoonCat Nose/Ears/Feet
119 - MoonCat Eyes
120 - MoonCat Complement 1
121 - MoonCat C1 Smoked Glass
122 - MoonCat C1 Stained Glass
123 - MoonCat C1 Tinted Glass
124 - MoonCat Complement 2
125 - MoonCat C2 Smoked Glass
126 - MoonCat C2 Stained Glass
127 - MoonCat C2 Tinted Glass

**/

/**
 * @title MoonCatColors
 * @notice On Chain MoonCat Palette Generation
 * @dev Provides On Chain Reference for the MoonCat and Accessory Colors
 */
interface IMoonCatColors {
    //address payable public owner;
    function owner() external view returns (address);

    //bool public finalized = false;
    function finalized() external view returns (bool);

    /**
     * @dev RGB color values for the 113 colors in the Accessories palette.
     */
    //uint8[339] public BasePalette;
    function BasePalette(uint256) external view returns (uint8);

    /**
     * @dev Convert a color from the RGB colorspace to HSL and return the Hue component.
     * Core function that was originally parsed in Javascript, translated to Solidity.
     */
    function RGBToHue (uint256 r, uint256 g, uint256 b) external pure returns (uint256);

    /**
     * @dev Convert a color from the HSL colorspace to RGB and return the Red, Green, and Blue components.
     * Core function that was originally parsed in Javascript, translated to Solidity.
     */
    function hueToRGB (uint256 hue, uint8 _lightness) external pure returns (uint8, uint8, uint8);

    /**
     * @dev For a given RGB color, derive a palette of six colors to be used to create the visual appearance of a MoonCat of that color.
     * Core function that was originally parsed in Javascript, translated to Solidity.
     */
    function deriveColors (uint8 red, uint8 green, uint8 blue, bool invert) external pure returns (uint8[24] memory);

    /**
     * @dev Add hard-coded color palettes for specific MoonCat hex IDs.
     *
     * Due to the differences between Javascript math and Solidity math, some MoonCat color conversions end up rounded differently
     * in the two languages. For the ones that cannot be calculated dynamically (edge-cases), they get hard-coded into the contract here.
     */
    function mapColors (bytes5[] calldata keys, uint128[] calldata vals) external;


    /**
     * @dev For a given MoonCat hex ID, return the RGB glow color for it.
     */
    function glowOf (bytes5 catId) external pure returns (uint8[3] memory);

    /**
     * @dev For a given MoonCat rescue order, return the RGB glow color for it.
     */
    function glowOf (uint256 rescueOrder) external view returns (uint8[3] memory);

    /**
     * @dev For a given MoonCat hex ID, return the 8 colors used to draw its main appearance and color-dependent Accessories.
     */
    function colorsOf (bytes5 catId) external view returns (uint8[24] memory);

    /**
     * @dev For a given MoonCat rescue order, return the 8 colors used to draw its main appearance and derive color-dependent Accessories.
     */
    function colorsOf (uint256 rescueOrder) external view returns (uint8[24] memory);

    /**
     * @dev For a given MoonCat hex ID, return the 15 colors used to draw its color-dependent Accessories.
     */
    function accessoryColorsOf(bytes5 catId) external view returns (uint8[45] memory);

    /**
     * @dev For a given MoonCat rescue order, return the 15 colors used to draw its color-dependent Accessories.
     */
    function accessoryColorsOf(uint256 rescueOrder) external view returns (uint8[45] memory);

    /**
     * @dev For a given index in the Accessory palette, return its alpha (translucency) value.
     */
    function colorAlpha (uint8 id) external pure returns (uint8);

    /**
     * @dev For a given MoonCat hex ID, return the 113 Accessory color palette, and the 15 colors specific to the MoonCat wearing the accessory, concatenated together.
     */
    function paletteOf(bytes5 catId) external view returns (uint8[384] memory);

    /**
     * @dev For a given MoonCat rescue order, return the 113 Accessory color palette, and the 15 colors specific to the MoonCat wearing the accessory, concatenated together.
     */
    function paletteOf(uint256 rescueOrder) external view returns (uint8[384] memory);

    /**
     * @dev For a given Hue degree value, return a human-friendly color name for that hue.
     */
    function hueName (uint16 hue) external pure returns (string memory);

    /**
     * @dev For a given MoonCat hex ID, return the Hue degree value for that MoonCat.
     */
    function hueIntOf (bytes5 catId) external view returns (uint16);

    /**
     * @dev For a given MoonCat rescue order, return the Hue degree value for that MoonCat.
     */
    function hueIntOf (uint256 rescueOrder) external view returns (uint16);

    /* General */

    /**
     * @dev Get documentation about this contract.
     */
    function doc() external view returns (string memory name, string memory description, string memory details);

    function finalize () external;

    /**
     * @dev Allow current `owner` to transfer ownership to another address.
     */
    function transferOwnership (address payable newOwner) external;

    /**
     * @dev Update the location of the Reference Contract.
     */
    function setReferenceContract (address referenceContract) external;

    /**
     * @dev Rescue ERC20 assets sent directly to this contract.
     */
    function withdrawForeignERC20 (address tokenContract) external;

    /**
     * @dev Rescue ERC721 assets sent directly to this contract.
     */
    function withdrawForeignERC721 (address tokenContract, uint256 tokenId) external;

}