// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

interface IMoonCatRescue {
  function mode () external view returns (uint8);

  function imageGenerationCodeMD5 () external view returns (bytes16);

  function name () external view returns (string memory);
  function symbol () external view returns (string memory);
  function decimals () external view returns (uint8);

  function totalSupply () external view returns (uint);
  function remainingCats () external view returns (uint16);
  function remainingGenesisCats () external view returns (uint16);
  function rescueIndex () external view returns (uint16);

  // bytes5[25600] public rescueOrder;
  function rescueOrder (uint256 order) external view returns (bytes5 catId);

  function searchSeed () external view returns (bytes32);

  function adoptionOffers (bytes5) external view returns (bool exists, bytes5 catId, address seller, uint256 price, address onlyOfferTo);
  function adoptionRequests (bytes5) external view returns (bool exists, bytes5 catId, address requester, uint256 price);

  function catNames (bytes5 catId) external view returns (bytes32);
  function catOwners (bytes5 catId) external view returns (address);
  function balanceOf (address) external view returns (uint256);
  function pendingWithdrawals (address) external view returns (uint256);

  /* events */
  event CatRescued(address indexed to, bytes5 indexed catId);
  event CatNamed(bytes5 indexed catId, bytes32 catName);
  event Transfer(address indexed from, address indexed to, uint256 value);
  event CatAdopted(bytes5 indexed catId, uint price, address indexed from, address indexed to);
  event AdoptionOffered(bytes5 indexed catId, uint price, address indexed toAddress);
  event AdoptionOfferCancelled(bytes5 indexed catId);
  event AdoptionRequested(bytes5 indexed catId, uint price, address indexed from);
  event AdoptionRequestCancelled(bytes5 indexed catId);
  event GenesisCatsAdded(bytes5[16] catIds);

  /* registers and validates cats that are found */
  function rescueCat (bytes32 seed) external returns (bytes5);

  /* assigns a name to a cat, once a name is assigned it cannot be changed */
  function nameCat (bytes5 catId, bytes32 catName) external;

  /* puts a cat up for anyone to adopt */
  function makeAdoptionOffer (bytes5 catId, uint price) external;

  /* puts a cat up for a specific address to adopt */
  function makeAdoptionOfferToAddress (bytes5 catId, uint price, address to) external;

  /* cancel an adoption offer */
  function cancelAdoptionOffer (bytes5 catId) external;

  /* accepts an adoption offer  */
  function acceptAdoptionOffer (bytes5 catId) external payable;

  /* transfer a cat directly without payment */
  function giveCat (bytes5 catId, address to) external;

  /* requests adoption of a cat with an ETH offer */
  function makeAdoptionRequest (bytes5 catId) external payable;

  /* allows the owner of the cat to accept an adoption request */
  function acceptAdoptionRequest (bytes5 catId) external;

  /* allows the requester to cancel their adoption request */
  function cancelAdoptionRequest (bytes5 catId) external;

  function withdraw () external;

  /* owner only functions */

  /* disable contract before activation. A safeguard if a bug is found before the contract is activated */
  function disableBeforeActivation () external;

  /* activates the contract in *Live* mode which sets the searchSeed and enables rescuing */
  function activate () external;

  /* activates the contract in *Test* mode which sets the searchSeed and enables rescuing */
  function activateInTestMode () external;

  /* add genesis cats in groups of 16 */
  function addGenesisCatGroup () external;


  /* aggregate getters */

  function getCatIds () external view returns (bytes5[] memory);

  function getCatNames () external view returns (bytes32[] memory);

  function getCatOwners () external view returns (address[] memory);

  function getCatOfferPrices () external view returns (uint[] memory);

  function getCatRequestPrices () external view returns (uint[] memory);

  function getCatDetails (bytes5 catId) external view returns (
    bytes5 id,
    address owner,
    bytes32 name,
    address onlyOfferTo,
    uint offerPrice,
    address requester,
    uint requestPrice
  );
}
