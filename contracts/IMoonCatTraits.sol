// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

/**
 * @title MoonCatTraits
 * @notice On Chain MoonCat Trait Parsing
 * @dev Provides On Chain Reference for the MoonCat Traits
 */
interface IMoonCatTraits {

    //address payable public owner;
    function owner() external view returns (address);

    /* Human-friendly trait names */

    //string[2] public facingNames = ["left", "right"];
    function facingNames(uint256) external view returns (string memory);

    //string[4] public expressionNames = ["smiling", "grumpy", "pouting", "shy"];
    function expressionNames(uint256) external view returns (string memory);

    //string[4] public patternNames = ["pure", "tabby", "spotted", "tortie"];
    function patternNames(uint256) external view returns (string memory);

    //string[4] public poseNames = ["standing", "sleeping", "pouncing", "stalking"];
    function poseNames(uint256) external view returns (string memory);

    /* Traits */

    /**
     * @dev For a given MoonCat rescue order, return the calendar year it was rescued in.
     */
    function rescueYearOf (uint256 rescueOrder) external pure returns (uint16);

    /**
     * @dev For a given MoonCat hex ID, extract the trait data from the "K" byte.
     */
    function kTraitsOf (bytes5 catId) external pure returns
        (bool genesis,
         bool pale,
         uint8 facing,
         uint8 expression,
         uint8 pattern,
         uint8 pose);

    /**
     * @dev For a given MoonCat rescue order, extract the trait data from the "K" byte.
     */
    function kTraitsOf (uint256 rescueOrder) external view returns
        (bool genesis,
         bool pale,
         uint8 facing,
         uint8 expression,
         uint8 pattern,
         uint8 pose);

    /**
     * @dev For a given MoonCat hex ID, extract the trait data in a human-friendly format.
     */
    function traitsOf (bytes5 catId) external view returns
        (bool genesis,
         bool pale,
         string memory facing,
         string memory expression,
         string memory pattern,
         string memory pose);

    /**
     * @dev For a given MoonCat rescue order, extract the trait data in a human-friendly format.
     */
    function traitsOf (uint256 rescueOrder) external view returns
        (bool genesis,
         bool pale,
         string memory facing,
         string memory expression,
         string memory pattern,
         string memory pose,
         bytes5 catId,
         uint16 rescueYear,
         bool isNamed
         );


    /**
     * @dev For a given MoonCat rescue order, return who owns that MoonCat.
     */
    function ownerOf (uint256 rescueOrder) external view returns (address);

    /**
     * @dev For a given MoonCat rescue order, return the hex ID of that MoonCat.
     */
    function catIdOf (uint256 rescueOrder) external view returns (bytes5);

    /**
     * @dev For a given MoonCat hex ID, return the recorded name of that MoonCat.
     */
    function nameOf (bytes5 catId) external view returns (string memory);

    /**
     * @dev For a given MoonCat rescue order, return the recorded name of that MoonCat.
     */
    function nameOf (uint256 rescueOrder) external view returns (string memory);

    /* General */

    /**
     * @dev Get documentation about this contract.
     */
    function doc() external view returns (string memory name, string memory description, string memory details);

    /**
     * @dev Allow current `owner` to transfer ownership to another address.
     */
    function transferOwnership (address payable newOwner) external;

    /**
     * @dev Update the ERC721 registry for a given address.
     */
    function setERC721Proxy (address proxyAddress, bool isProxy) external;

    /**
     * @dev Update the location of the Reference Contract.
     */
    function setReferenceContract (address referenceContract) external;

    /**
     * @dev Rescue ERC20 assets sent directly to this contract.
     */
    function withdrawForeignERC20(address tokenContract) external;

    /**
     * @dev Rescue ERC721 assets sent directly to this contract.
     */
    function withdrawForeignERC721(address tokenContract, uint256 tokenId) external;

}