// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

interface IMoonCatAccessories {
    struct OwnedAccessory {
        uint232 accessoryId;
        uint8 paletteIndex;
        uint16 zIndex;
    }
}

/**
 * @title AccessoryPNGs
 * @notice On Chain MoonCat Accessory Image Generation
 * @dev Builds PNGs of MoonCat Accessories
 */
interface IMoonCatAccessoryImages {

    //address payable public owner;
    function owner() external view returns (address);

    //uint64 constant public PNGHeader = 0x89504e470d0a1a0a;
    function PNGHeader() external view returns (uint64);

    //uint96 constant public PNGFooter = 0x0000000049454e44ae426082;
    function PNGFooter() external view returns (uint96);

    //uint40 constant internal IHDRDetails = 0x0803000000;
    function IHDRDetails() external view returns (uint40);


    /**
     * @dev Create a cyclic redundancy check (CRC) value for a given set of data.
     *
     * This is the error-detecting code used for the PNG data format to validate each chunk of data within the file. This Solidity implementation
     * is needed to be able to dynamically create PNG files piecemeal.
     */
    function crc32 (bytes memory data) external view returns (uint32);

    /* accessoryPNGs */

    /**
     * @dev Assemble a block of data into a valid PNG file chunk.
     */
    function generatePNGChunk (string memory typeCode, bytes memory data) external view returns (bytes memory);

    /**
     * @dev For a given MoonCat rescue order and Accessory ID and palette ID, render as PNG.
     * The PNG output is converted to a base64-encoded blob, which is the format used for encoding into an SVG or inline HTML.
     */
    function accessoryPNG (uint256 rescueOrder, uint256 accessoryId, uint16 paletteIndex) external view returns (string memory);

    /* Composite */

    struct PreppedAccessory {
        uint16 zIndex;

        uint8 offsetX;
        uint8 offsetY;
        uint8 width;
        uint8 height;

        bool mirror;
        bool background;

        bytes8 palette;
        bytes IDAT;
    }

    /**
     * @dev Given a MoonCat and a set of basic Accessories' information, derive their metadata and split into foreground/background lists.
     */
    function prepAccessories (
      uint256 rescueOrder,
      uint8 facing,
      uint8 pose,
      bool allowUnverified,
      IMoonCatAccessories.OwnedAccessory[] memory accessories
    ) external view returns (PreppedAccessory[] memory, PreppedAccessory[] memory);


    /**
     * @dev Given a MoonCat Rescue Order and a list of Accessories they own, render an SVG of them wearing those accessories.
     */
    function accessorizedImageOf (uint256 rescueOrder, IMoonCatAccessories.OwnedAccessory[] memory accessories, uint8 glowLevel, bool allowUnverified)
        external
        view
        returns (string memory);

    /**
     * @dev Given a MoonCat Rescue Order, look up what Accessories they are currently wearing, and render an SVG of them wearing those accessories.
     */
    function accessorizedImageOf (uint256 rescueOrder, uint8 glowLevel, bool allowUnverified)
        external
        view
        returns (string memory);

    /**
     * @dev Given a MoonCat Rescue Order, look up what verified Accessories they are currently wearing, and render an SVG of them wearing those accessories.
     */
    function accessorizedImageOf (uint256 rescueOrder, uint8 glowLevel)
        external
        view
        returns (string memory);

    /**
     * @dev Given a MoonCat Rescue Order, look up what verified Accessories they are currently wearing, and render an unglowing SVG of them wearing those accessories.
     */
    function accessorizedImageOf (uint256 rescueOrder)
        external
        view
        returns (string memory);

    /**
     * @dev Given a MoonCat Rescue Order and an Accessory ID, return the bounding box of the Accessory, relative to the MoonCat.
     */
    function placementOf (uint256 rescueOrder, uint256 accessoryId)
        external
        view
        returns (uint8 offsetX, uint8 offsetY, uint8 width, uint8 height, bool mirror, bool background);

    /* General */

    /**
     * @dev Get documentation about this contract.
     */
    function doc() external view returns (string memory name, string memory description, string memory details);

    /**
     * @dev Allow current `owner` to transfer ownership to another address.
     */
    function transferOwnership (address payable newOwner) external;

    /**
     * @dev Update the location of the Reference Contract.
     */
    function setReferenceContract (address referenceContract) external;

    /**
     * @dev Rescue ERC20 assets sent directly to this contract.
     */
    function withdrawForeignERC20(address tokenContract) external;

    /**
     * @dev Rescue ERC721 assets sent directly to this contract.
     */
    function withdrawForeignERC721(address tokenContract, uint256 tokenId) external;
}