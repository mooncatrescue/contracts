// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

interface IJumpPort {
  // bool public depositPaused;
  function depositPaused () external view returns (bool);

  // bytes32 public constant PORTAL_ROLE = keccak256("PORTAL_ROLE");
  function PORTAL_ROLE () external view returns (bytes32);

  // mapping(address => bool) public lockOverride;
  function lockOverride (address) external view returns (bool);

  // mapping(address => bool) public executionBlocked;
  function executionBlocked (address) external view returns (bool);

  struct LockRecord {
    address parentLock;
    bool isLocked;
  }

  /**
   * @dev Record an Owner's collection balance, and the block it was last updated.
   *
   * Stored as less than uint256 values to fit into one storage slot.
   *
   * This structure will work until approximately the year 2510840694154672305534315769283066566440942177785607
   * (when the block height becomes too large for a uint192), and for oners who don't have more than
   * 18,​446,​744,​073,​709,​551,​615 items from a single collection deposited.
   */
  struct BalanceRecord {
    uint64 balance;
    uint192 blockHeight;
  }

  event Deposit(address indexed owner, address indexed tokenAddress, uint256 indexed tokenId);
  event Withdraw(address indexed owner, address indexed tokenAddress, uint256 indexed tokenId, uint256 duration);
  event Approval(address indexed owner, address indexed approved, address indexed tokenAddress, uint256 tokenId);
  event ApprovalForAll(address indexed owner, address indexed operator, bool approved);
  event Lock(address indexed portalAddress, address indexed owner, address indexed tokenAddress, uint256 tokenId);
  event Unlock(address indexed portalAddress, address indexed owner, address indexed tokenAddress, uint256 tokenId);
  event ActionExecuted(address indexed tokenAddress, uint256 indexed tokenId, address target, bytes data);

  /**
   * @dev Receive a token directly; transferred with the `safeTransferFrom` method of another ERC721 token.
   * @param operator the _msgSender of the transaction
   * @param from the address of the former owner of the incoming token
   * @param tokenId the ID of the incoming token
   * @param data additional metdata
   */
  function onERC721Received (
    address operator,
    address from,
    uint256 tokenId,
    bytes calldata data
  ) external returns (bytes4);

  /**
   * @dev Deposit an individual token from a specific collection.
   * To be successful, the JumpPort contract must be "Approved" to move this token on behalf
   * of the current owner, in the token's contract.
   */
  function deposit (address tokenAddress, uint256 tokenId) external;

  /**
   * @dev Deposit multiple tokens from a single collection.
   * To be successful, the JumpPort contract must be "Approved" to move these tokens on behalf
   * of the current owner, in the token's contract.
   */
  function deposit (address tokenAddress, uint256[] calldata tokenIds) external;

  /**
   * @dev Deposit multiple tokens from multiple different collections.
   * To be successful, the JumpPort contract must be "Approved" to move these tokens on behalf
   * of the current owner, in the token's contract.
   */
  function deposit (address[] calldata tokenAddresses, uint256[] calldata tokenIds) external;

  /* Withdraw Tokens */

  /**
   * @dev Withdraw a token, to the owner's address, using `safeTransferFrom`, with no additional data.
   */
  function safeWithdraw (address tokenAddress, uint256 tokenId) external;

  /**
   * @dev Withdraw a token, to the owner's address, using `safeTransferFrom`, with additional calldata.
   */
  function safeWithdraw (address tokenAddress, uint256 tokenId, bytes calldata data) external;

  /**
   * @dev Bulk withdraw multiple tokens, using `safeTransferFrom`, with no additional data.
   */
  function safeWithdraw (address[] calldata tokenAddresses, uint256[] calldata tokenIds) external;

  /**
   * @dev Bulk withdraw multiple tokens, using `safeTransferFrom`, with additional calldata.
   */
  function safeWithdraw (address[] calldata tokenAddresses, uint256[] calldata tokenIds, bytes[] calldata data) external;

  /**
   * @dev Withdraw a token, to the owner's address, using `transferFrom`.
   * USING `transferFrom` RATHER THAN `safeTransferFrom` COULD RESULT IN LOST TOKENS. USE `safeWithdraw` FUNCTIONS
   * WHERE POSSIBLE, OR DOUBLE-CHECK RECEIVING ADDRESSES CAN HOLD TOKENS IF USING THIS FUNCTION.
   */
  function withdraw (address tokenAddress, uint256 tokenId) external;

  /**
   * @dev Bulk withdraw multiple tokens, to specific addresses, using `transferFrom`.
   * USING `transferFrom` RATHER THAN `safeTransferFrom` COULD RESULT IN LOST TOKENS. USE `safeWithdraw` FUNCTIONS
   * WHERE POSSIBLE, OR DOUBLE-CHECK RECEIVING ADDRESSES CAN HOLD TOKENS IF USING THIS FUNCTION.
   */
  function withdraw (address[] calldata tokenAddresses, uint256[] calldata tokenIds) external;

  /**
   * @dev Designate another address that can act on behalf of this token.
   * This allows the Copliot address to withdraw the token from the JumpPort, and interact with any Portal
   * on behalf of this token.
   */
  function setCopilot (address copilot, address tokenAddress, uint256 tokenId) external;

  /**
   * @dev Designate another address that can act on behalf of all tokens owned by the sender's address.
   * This allows the Copliot address to withdraw the token from the JumpPort, and interact with any Portal
   * on behalf of any token owned by the sender.
   */
  function setCopilotForAll (address copilot, bool approved) external;

  /* Receive actions from Portals */

  /**
   * @dev Lock a token into the JumpPort.
   * Causes a token to not be able to be withdrawn by its Owner, until the same Portal contract calls the `unlockToken` function for it,
   * or the locks for that portal are all marked as invalid (either by JumpPort administrators, or the Portal itself).
   */
  function lockToken (address tokenAddress, uint256 tokenId) external;

  /**
   * @dev Unlocks a token held in the JumpPort.
   * Does not withdraw the token from the JumpPort, but makes it available for withdraw whenever the Owner wishes to.
   */
  function unlockToken (address tokenAddress, uint256 tokenId) external;

  /**
   * @dev Take an action as the JumpPort (the owner of the tokens within it), as directed by a Portal.
   * This is a powerful function and Portals that wish to use it NEEDS TO MAKE SURE its execution is guarded by
   * checks to ensure the address passed as `operator` to this function is the one authorizing the action
   * (in most cases, it should be the `msg.sender` communicating to the Portal), and that the `payload`
   * being passed in operates on the `tokenId` indicated, and no other tokens.
   *
   * Here on the JumpPort side, it verifies that the `operator` passed in is the current owner or a Copilot of the
   * token being operated upon, but has to trust the Portal that the passed-in `tokenId` matches what token
   * will get acted upon in the `payload`.
   */
  function executeAction (address operator, address tokenAddress, uint256 tokenId, address targetAddress, bytes calldata payload) external;

  /**
   * @dev Unlocks all locks held by a Portal.
   * Intended to be called in the situation of a large failure of an individual Portal's operation,
   * as a way for the Portal itself to indicate it has failed, and all tokens that were previously
   * locked by it should be allowed to exit.
   *
   * This function only allows Portals to enable/disable the locks they created. The function `setAdminLockOverride`
   * is similar, but allows JumpPort administrators to set/clear the lock ability for any Portal contract.
   */
  function unlockAllTokens (bool isOverridden) external;

  /**
   * @dev Prevent a Portal from executing calls to other contracts.
   * Intended to be called in the situation of a large failure of an individual Portal's operation,
   * as a way for the Portal itself to indicate it has failed, and arbitrary contract calls should not
   * be allowed to originate from it.
   *
   * This function only allows Portals to enable/disable their own execution right. The function `setAdminExecutionBlocked`
   * is similar, but allows JumpPort administrators to set/clear the execution block for any Portal contract.
   */
  function blockExecution (bool isBlocked) external;

  /* View functions */

  /**
   * @dev Is the specified token currently deposited in the JumpPort?
   */
  function isDeposited (address tokenAddress, uint256 tokenId) external view returns (bool);


  /**
   * @dev When was the specified token deposited in the JumpPort?
   */
  function depositedSince (address tokenAddress, uint256 tokenId) external view returns (uint256 blockNumber);

  /**
   * @dev Is the specified token currently locked in the JumpPort?
   * If any Portal contract has a valid lock (the Portal has indicated the token should be locked,
   * and the Portal's locking rights haven't been overridden) on the token, this function will return true.
   */
  function isLocked (address tokenAddress, uint256 tokenId) external view returns (bool);

  /**
   * @dev Get a list of all Portal contract addresses that hold a valid lock (the Portal has indicated the token should be locked,
   * and the Portal's locking rights haven't been overridden) on the token.
   */
  function lockedBy (address tokenAddress, uint256 tokenId) external view returns (address[] memory);

  /**
   * @dev Who is the owner of the specified token that is deposited in the JumpPort?
   * A core tenent of the JumpPort is that this value will not change while the token is deposited;
   * a token cannot change owners while in the JumpPort, though they can add/remove Copilots.
   */
  function ownerOf (address tokenAddress, uint256 tokenId) external view returns (address owner);

  /**
   * @dev Who are the owners of a specified range of tokens in a collection?
   * Bulk query function, to be able to enumerate a whole token collection more easily on the client end
   */
  function ownersOf (address tokenAddress, uint256 tokenSearchStart, uint256 tokenSearchEnd) external view returns (address[] memory tokenOwners);

  /**
   * @dev For a specified owner address, what tokens in the specified range do they own?
   * Bulk query function, to be able to enumerate a specific address' collection more easily on the client end
   */
  function ownedTokens (address tokenAddress, address owner, uint256 tokenSearchStart, uint256 tokenSearchEnd) external view returns (uint256[] memory tokenIds);

  /**
   * @dev For a specific token collection, how many tokens in that collection does a specific owning address own in the JumpPort?
   */
  function balanceOf (address tokenAddress, address owner) external view returns (BalanceRecord memory);

  /**
   * @dev For a specific set of token collections, how many tokens total does a specific owning address own in the JumpPort?
   * Bulk query function, to be able to enumerate a specific address' collection more easily on the client end
   */
  function balanceOf (address[] calldata tokenAddresses, address owner) external view returns (uint256);

  /**
   * @dev For a specific token, which other address is approved to act as the owner of that token for actions pertaining to the JumpPort?
   */
  function getApproved (address tokenAddress, uint256 tokenId) external view returns (address copilot);

  /**
   * @dev For a specific owner's address, is the specified operator address allowed to act as the owner for actions pertaining to the JumpPort?
   */
  function isApprovedForAll (address owner, address operator) external view returns (bool);

  /* Administration */

  /**
   * @dev Add or remove the "Portal" role to a specified address.
   */
  function setPortalValidation (address portalAddress, bool isValid) external;

  /**
   * @dev Prevent new tokens from being added to the JumpPort.
   */
  function setPaused (bool isDepositPaused) external;

  /**
   * @dev As an administrator of the JumpPort contract, set a Portal's locks to be valid or not.
   *
   * This function allows JumpPort administrators to set/clear the override for any Portal contract.
   * The `unlockAllTokens` function is similar (allowing Portal addresses to set/clear lock overrides as well)
   * but only for their own Portal address.
   */
  function setAdminLockOverride (address portal, bool isOverridden) external;

  /**
   * @dev As an administrator of the JumpPort contract, set a Portal to be able to execute other functions or not.
   *
   * This function allows JumpPort administrators to set/clear the execution block for any Portal contract.
   * The `blockExecution` function is similar (allowing Portal addresses to set/clear lock overrides as well)
   * but only for their own Portal address.
   */
  function setAdminExecutionBlocked (address portal, bool isBlocked) external;

  /**
   * @dev Contract owner requesting the owner of a token check in.
   * This starts the process of the owner of the contract being able to remove any token, after a time delay.
   * If the current owner does not want the token removed, they have 2,400,000 blocks (about one year)
   * to trigger the `ownerPong` method, which will abort the withdraw
   */
  function adminWithdrawPing (address tokenAddress, uint256 tokenId) external;

  /**
   * @dev As the owner of a token, abort an attempt to force-remove it.
   * The owner of the contract can remove any token from the JumpPort, if they trigger the `adminWithdrawPing` function
   * for that token, and the owner does not respond by calling this function within 2,400,000 blocks (about one year)
   */
  function ownerPong (address tokenAddress, uint256 tokenId) external;

  /**
   * @dev As an Administrator, abort an attempt to force-remove a token.
   * This is a means for the Administration to change its mind about a force-withdraw, or to correct the actions of a rogue Administrator.
   */
  function adminPong (address tokenAddress, uint256 tokenId) external;

  /**
   * @dev Check if a token has a ping from the contract Administration pending, and if so, what block it was requested at
   * Returns zero if there is no request pending.
   */
  function tokenPingRequestBlock (address tokenAddress, uint256 tokenId) external view returns (uint256 blockNumber);

  /**
   * @dev Check if a set of tokens have a ping from the contract Administration pending, and if so, what block it was requested at
   * Returns zero for a token if there is no request pending for that token.
   */
  function tokenPingRequestBlocks (address[] calldata tokenAddresses, uint256[] calldata tokenIds) external view returns(uint256[] memory blockNumbers);

  /**
   * @dev Rescue ERC721 assets sent directly to this contract.
   */
  function withdrawForeignERC721 (address tokenContract, uint256 tokenId) external;

  /* OwnableBase */

  error MissingRole(bytes32 role, address operator);

  // bytes32 public constant ADMIN_ROLE = 0x00;
  function ADMIN_ROLE () external view returns (bytes32);

  // IDocumentationRepository public DocumentationRepository;
  function DocumentationRepository () external view returns (address);

  event RoleChange (bytes32 indexed role, address indexed account, bool indexed isGranted, address sender);

  function doc () external view returns (string memory name, string memory description, string memory details);

  /**
   * @dev See {ERC1271-isValidSignature}.
   */
  function isValidSignature(bytes32 hash, bytes memory) external view returns (bytes4 magicValue);

  /**
   * @dev Inspect whether a specific address has a specific role.
   */
  function hasRole (bytes32 role, address account) external view returns (bool);

  /**
   * @dev Allow current administrators to be able to grant/revoke admin role to other addresses.
   */
  function setAdmin (address account, bool isAdmin) external;

  /**
   * @dev Claim ENS reverse-resolver rights for this contract.
   * https://docs.ens.domains/contract-api-reference/reverseregistrar#claim-address
   */
  function setReverseResolver (address registrar) external;

  /**
   * @dev Set a message as valid, to be queried by ERC1271 clients.
   */
  function markMessageSigned (bytes32 hash, uint256 expirationLength) external;

  /**
   * @dev Rescue ERC20 assets sent directly to this contract.
   */
  function withdrawForeignERC20 (address tokenContract) external;
}