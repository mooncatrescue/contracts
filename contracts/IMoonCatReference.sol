// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

/**
 * @title MoonCatsOnChain
 * @notice On Chain Reference for Offical MoonCat Projects
 * @dev Maintains a mapping of contract addresses to documentation/description strings
 */
interface IMoonCatReference {

    /* Original MoonCat Rescue Contract */
    //address constant public MoonCatRescue = 0x60cd862c9C687A9dE49aecdC3A99b74A4fc54aB6;
    function MoonCatRescue() external view returns (address);

    //address payable public owner;
    function owner() external view returns (address);

    /* Documentation */

    struct Doc {
        string name;
        string description;
        string details;
    }

    /**
     * @dev How many Contracts does this Reference contract have documentation for?
     */
    function totalContracts () external view returns (uint256);

    /**
     * @dev Iterate through the addresses this Reference contract has documentation for.
     */
    function contractAddressByIndex (uint256 index) external view returns (address);

    /**
     * @dev For a specific address, get the details this Reference contract has for it.
     */
    function doc (address _contractAddress) external view returns (string memory name, string memory description, string memory details);

    /**
     * @dev Iterate through the addresses this Reference contract has documentation for, returning the details stored for that contract.
     */
    function doc (uint256 index) external view returns (string memory name, string memory description, string memory details, address contractAddress);

    /**
     * @dev Get documentation about this contract.
     */
    function doc () external view returns (string memory name, string memory description, string memory details);

    /**
     * @dev Update the stored details about a specific Contract.
     */
    function setDoc (address contractAddress, string memory name, string memory description, string memory details) external;

    /**
     * @dev Update the name and description about a specific Contract.
     */
    function setDoc (address contractAddress, string memory name, string memory description) external;

    /**
     * @dev Update the details about a specific Contract.
     */
    function updateDetails (address contractAddress, string memory details) external;

    /**
     * @dev Update the details for multiple Contracts at once.
     */
    function batchSetDocs (address[] calldata contractAddresses, Doc[] calldata docs) external;

    /**
     * @dev Allow current `owner` to transfer ownership to another address
     */
    function transferOwnership (address payable newOwner) external;

    /**
     * @dev Rescue ERC20 assets sent directly to this contract.
     */
    function withdrawForeignERC20 (address tokenContract) external;

    /**
     * @dev Rescue ERC721 assets sent directly to this contract.
     */
    function withdrawForeignERC721 (address tokenContract, uint256 tokenId) external;
}