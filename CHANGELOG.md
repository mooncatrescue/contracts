# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.0.0] - 2025-03-03

### Added

- Interface for MoonCatNameService (MCNS)
- Descriptions and deployment transaction identifiers for each contract
- `generateDocs` script to update README file with contract info

## [2.0.1] - 2024-09-23

### Added

- Typescript type annotations for better IDE integration.

### Fixed

- Broken functions that were still depending on Ethers.
- Mislabeled parameter in lootprints contract metadata.

## [2.0.0] - 2024-02-21

### Changed

- Removed Ethers as a dependency. This module no longer returns Ethers "Contract" objects, but more simplified metadata about each smart contract in the ecosystem.

## [1.3.0] - 2024-02-21

### Added

- Added MoonCatMoments contract.
- Added MoonCatTraits, MoonCatColors, MoonCatSVGs, MoonCatAccessoryImages, and MoonCatReference contracts.

## [1.2.1] - 2023-07-06

### Fixed

- Added in missing Events from MoonCatAcclimator interface.
- Added missing RoleChange Event to OwnableBase construction.

## [1.2.0] - 2023-03-23

### Changed

- Made Hardhat an optional dependency; used Ethers directly where possible.

## [1.1.1] - 2022-02-23

### Added

- JumpPort Javascript interfaces.

## [1.0.4] - 2022-09-21

### Fixed

- Add README to deployed package, as a copy of the README in the root of the repository.

## [1.0.3] - 2022-09-21

### Added

- Initialized repository as NPM module, and stabilized publishing infrastructure for it.
- Added Solidity interface files for existing MoonCat-related contracts.
- Added Hardhat configuration to move `artifacts` folder inside the `contracts` folder, such that they could be published/bundled together.
